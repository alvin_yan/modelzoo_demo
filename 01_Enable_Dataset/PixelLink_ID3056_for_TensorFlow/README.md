# PixelLink 使用Dataset预处理方式训练

## 前置条件

### 下载数据集

下载 [ICDAR2015](http://rrc.cvc.uab.es/?ch=4&com=downloads)数据集

将数据集放在./datasets/icdar2015 路径下

### 制作tfrecord文件

使用./datasets/icdar2015_to_tfrecords.py

输出的tfrecord会保存在 ./datasets/ICDAR2015 路径下


## 模型训练

```
python train_pixel_link.py --dataset_dir=${dataset_path} --train_dir=${output_path}
```

train_dir用于保存输出的ckpt文件

使用ModelArts训练：

```
python modelarts_entry.py --data_url=${dataset_path} --train_url=${output_path}
```
推荐使用V100（32GB）


## 模型验证


```
python test_pixel_link.py --dataset_dir=${dataset_path} --checkpoint_path=${ckpt_path}
```
注意：这里的dataset_path应当指向 下载好的icdar_2015/ch4_test_images这个文件夹

运行结束之后将生成的txt文件夹打包成zip格式
使用./evaluation/script.py 进行模型精度数据的验证

```
python script.py –g=gt.zip –s=txt.zip –o=./, -p={\"IOU_CONSTRAINT\":0.5}
```


## 参考结果

GPU复现结果：
"precision": 0.8311242022582229, "recall": 0.8151179585941262, "hmean": 0.8230432668935342

使用Dataset预处理方式训练的结果：
"precision": 0.8353808353808354, "recall": 0.8184882041405874, "hmean": 0.8268482490272374