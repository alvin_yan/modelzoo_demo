# PSPNet-Reproduce 使用Dataset预处理方式训练




# 骨架模型

ResNet-V1-101

# 数据集

 [Cityscapes](https://www.cityscapes-dataset.com/).



# 数据集预处理

生成 `*labelTrainIds.png` 图片参考[createTrainIdLabelImgs](https://github.com/mcordts/cityscapesScripts/blob/master/cityscapesscripts/preparation/createTrainIdLabelImgs.py).

# 下载预训练模型

```bash
cd z_pretrained_weights
sh download_resnet_v1_101.sh
```

# 单卡训练

```bash
python ./run.py --network 'resnet_v1_101' --visible_gpus '0' --reader_method 'tf.data' --lrn_rate 0.01 --weight_decay_mode 1 --weight_decay_rate 0.0001 --weight_decay_rate2 0.001 --database 'Cityscapes' --subsets_for_training 'train' --batch_size 3 --train_image_size 816 --snapshot 15000 --train_max_iter 150000 --test_image_size 816 --random_rotate 0 --fine_tune_filename 'z_pretrained_weights/resnet_v1_101.ckpt'
```

# 参考结果

mIoU 77.09, [日志文件](https://gitee.com/zero167/modelzoo_demo/blob/master/01_Enable_Dataset/PSPNet-TF-Reproduce/MA_log/modelarts-job-8d3b8b70-6aa0-4b16-a422-bff25ba1b058-worker-0.log)



