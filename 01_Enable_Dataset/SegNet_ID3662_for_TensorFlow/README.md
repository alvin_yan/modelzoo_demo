# SegNet 使用Dataset预处理方式训练

## 前置条件

### 下载数据集

下载 [CamVid](https://github.com/alexgkendall/SegNet-Tutorial/tree/master/CamVid)数据集到./CamVid路径下

### 制作tfrecord文件

1. 手动制作：使用Input.py中image2tfrecords()函数将CamVid中image和label图片存储为bytes格式，制作tfrecord文件。

命令：
```bash
python Inputs.py --data_path="CamVid/train" --label_dir="CamVid/trainannot" --tf_record_path="CamVid/tfrecord/trainrecord"
```

2. 自动创建：main.py训练参数初始化时若不存在tfrecord文件，将通过Input.initial()函数自动构建图片对应的tfrecord，存储于CamVid/tfrecord路径下。


## 模型训练

```bash
python main.py --tfrecord_path='CamVid/tfrecord' --batch_size=5 --max_steps=40000
```

## 参考结果
ACC:86.66 Mean_IU:52.82  [日志文件](https://gitee.com/songxt3/modelzoo_demo/blob/master/01_Enable_Dataset/SegNet_TensorFlow/SegNet_log/events.out.tfevents.1658149195.cuda1)

## 脚本和示例代码

```
├── main.py                                  //参数初始化
├── model.py                      	     //模型训练代码
├── README.md                                //代码说明文档
├── Inputs.py                  		     //tfrecord数据集创建、数据读取
├── Utils.py                                 // 图片及数据基本操作

```