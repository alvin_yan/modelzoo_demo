-   [基本信息](#基本信息.md)
-   [概述](#概述.md)
-   [训练环境准备](#训练环境准备.md)
-   [快速上手](#快速上手.md)
-   [迁移学习指导](#迁移学习指导.md)
-   [高级参考](#高级参考.md)

<h2 id="基本信息.md">基本信息</h2>

**发布者（Publisher）：Huawei**

**应用领域（Application Domain）：** 3D point cloud segmentation 

**版本（Version）：1.2**

**修改时间（Modified） ：2021.11.3**

**大小（Size）：6.91MB**

**框架（Framework）：TensorFlow 1.15.0**

**模型格式（Model Format）：ckpt**

**精度（Precision）：**

**处理器（Processor）：昇腾910**

**应用级别（Categories）：Official**

**描述（Description）：基于TensorFlow框架的squeezeseg三维点云实例和语义分割网络训练代码** 

<h2 id="概述.md">概述</h2>

提出了一个相对简单且通用的3d点云实例分割网路3D-Bonet，此网络是一个单阶段、无锚的端到端网络，不需要做后处理步骤，运行效率大大提高。

- 参考论文：
    [https://arxiv.org/abs/1906.01140)

- 参考实现：

    [[Yang7879/3D-BoNet: 🔥3D-BoNet in Tensorflow (NeurIPS 2019, Spotlight) (github.com)](https://github.com/Yang7879/3D-BoNet))

- 适配昇腾 AI 处理器的实现：

  

  ​    


- 通过Git获取对应commit\_id的代码方法如下：

  ```
  git clone {repository_url}    # 克隆仓库的代码
  cd {repository_name}    # 切换到模型的代码仓目录
  git checkout  {branch}    # 切换到对应分支
  git reset --hard ｛commit_id｝     # 代码设置到对应的commit_id
  cd ｛code_path｝    # 切换到模型代码所在路径，若仓库下只有该模型，则无需切换
  ```

## 默认配置<a name="section91661242121611"></a>

- 训练超参

  - Batch size: 4
  - Train epoches: 50


## 支持特性<a name="section1899153513554"></a>

| 特性列表   | 是否支持 |
| ---------- | -------- |
| 分布式训练 | 否     |
| 混合精度   | 是       |
| 并行数据   | 是       |

## 混合精度训练<a name="section168064817164"></a>

昇腾910 AI处理器提供自动混合精度功能，可以针对全网中float32数据类型的算子，按照内置的优化策略，自动将部分float32的算子降低精度到float16，从而在精度损失很小的情况下提升系统性能并减少内存使用。

## 开启混合精度<a name="section20779114113713"></a>

脚本已默认开启混合精度，设置precision_mode参数的脚本参考如下。

  ```
在train.py中添加改行代码：
custom_op.parameter_map["precision_mode"].s = tf.compat.as_bytes("allow_mix_precision")
  ```


<h2 id="训练环境准备.md">训练环境准备</h2>

1. 硬件环境准备请参见各硬件产品文档"[驱动和固件安装升级指南]( https://support.huawei.com/enterprise/zh/category/ai-computing-platform-pid-1557196528909)"。需要在硬件设备上安装与CANN版本配套的固件与驱动。

2. 宿主机上需要安装Docker并登录[Ascend Hub中心](https://ascendhub.huawei.com/#/detail?name=ascend-tensorflow-arm)获取镜像。

   当前模型支持的镜像列表如[表1](#zh-cn_topic_0000001074498056_table1519011227314)所示。

   **表 1** 镜像列表

   <a name="zh-cn_topic_0000001074498056_table1519011227314"></a>

   <table><thead align="left"><tr id="zh-cn_topic_0000001074498056_row0190152218319"><th class="cellrowborder" valign="top" width="47.32%" id="mcps1.2.4.1.1"><p id="zh-cn_topic_0000001074498056_p1419132211315"><a name="zh-cn_topic_0000001074498056_p1419132211315"></a><a name="zh-cn_topic_0000001074498056_p1419132211315"></a><em id="i1522884921219"><a name="i1522884921219"></a><a name="i1522884921219"></a>镜像名称</em></p>
   </th>
   <th class="cellrowborder" valign="top" width="25.52%" id="mcps1.2.4.1.2"><p id="zh-cn_topic_0000001074498056_p75071327115313"><a name="zh-cn_topic_0000001074498056_p75071327115313"></a><a name="zh-cn_topic_0000001074498056_p75071327115313"></a><em id="i1522994919122"><a name="i1522994919122"></a><a name="i1522994919122"></a>镜像版本</em></p>
   </th>
   <th class="cellrowborder" valign="top" width="27.16%" id="mcps1.2.4.1.3"><p id="zh-cn_topic_0000001074498056_p1024411406234"><a name="zh-cn_topic_0000001074498056_p1024411406234"></a><a name="zh-cn_topic_0000001074498056_p1024411406234"></a><em id="i723012493123"><a name="i723012493123"></a><a name="i723012493123"></a>配套CANN版本</em></p>
   </th>
   </tr>
   </thead>
   <tbody><tr id="zh-cn_topic_0000001074498056_row71915221134"><td class="cellrowborder" valign="top" width="47.32%" headers="mcps1.2.4.1.1 "><a name="zh-cn_topic_0000001074498056_ul81691515131910"></a><a name="zh-cn_topic_0000001074498056_ul81691515131910"></a><ul id="zh-cn_topic_0000001074498056_ul81691515131910"><li><em id="i82326495129"><a name="i82326495129"></a><a name="i82326495129"></a>ARM架构：<a href="https://ascend.huawei.com/ascendhub/#/detail?name=ascend-tensorflow-arm" target="_blank" rel="noopener noreferrer">ascend-tensorflow-arm</a></em></li><li><em id="i18233184918125"><a name="i18233184918125"></a><a name="i18233184918125"></a>x86架构：<a href="https://ascend.huawei.com/ascendhub/#/detail?name=ascend-tensorflow-x86" target="_blank" rel="noopener noreferrer">ascend-tensorflow-x86</a></em></li></ul>
   </td>
   <td class="cellrowborder" valign="top" width="25.52%" headers="mcps1.2.4.1.2 "><p id="zh-cn_topic_0000001074498056_p1450714271532"><a name="zh-cn_topic_0000001074498056_p1450714271532"></a><a name="zh-cn_topic_0000001074498056_p1450714271532"></a><em id="i72359495125"><a name="i72359495125"></a><a name="i72359495125"></a>20.2.0</em></p>
   </td>
   <td class="cellrowborder" valign="top" width="27.16%" headers="mcps1.2.4.1.3 "><p id="zh-cn_topic_0000001074498056_p18244640152312"><a name="zh-cn_topic_0000001074498056_p18244640152312"></a><a name="zh-cn_topic_0000001074498056_p18244640152312"></a><em id="i162363492129"><a name="i162363492129"></a><a name="i162363492129"></a><a href="https://support.huawei.com/enterprise/zh/ascend-computing/cann-pid-251168373/software" target="_blank" rel="noopener noreferrer">20.2</a></em></p>
   </td>
   </tr>
   </tbody>
   </table>


<h2 id="快速上手.md">快速上手</h2>

- 数据集准备

1. S3DIS: https://drive.google.com/open?id=1hOsoOqOWKSZIgAZLu2JmOb_U8zdR04v0

1. 百度盘: https://pan.baidu.com/s/1ww_Fs2D9h7_bA2HfNIa2ig 密码:qpt7


## 模型训练<a name="section715881518135"></a>

- 单击“立即下载”，并选择合适的下载方式下载源码包。

- 启动训练之前，首先要配置程序运行相关环境变量。

  环境变量配置信息参见：

     [Ascend 910训练平台环境变量设置](https://gitee.com/ascend/modelzoo/wikis/Ascend%20910%E8%AE%AD%E7%BB%83%E5%B9%B3%E5%8F%B0%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F%E8%AE%BE%E7%BD%AE?sort_id=3148819)

- 单卡训练 

  1. 配置训练参数。

     首先在脚本train.py中，配置训练数据集路径，请用户根据实际路径配置，数据集参数如下所示：

     ```
     #在train.py中修改FLAGS中的信息：
     
     #将data_path改成对应的数据集存放位置（若在modelarts上运行则无需修改此路径）
     tf.app.flags.DEFINE_string('data_path', '/home/ma-user/modelarts/inputs/data_url_0/', """Root directory of data""")
     
     #将output_path改成对应的模型存放位置（若在modelarts上运行则无需修改此路径）
     tf.app.flags.DEFINE_string('output_path', '/home/ma-user/modelarts/outputs/train_url_0/',
                                """Directory where to write event logs and checkpoint. """)
     #需要训练的最大epoch数：
     tf.app.flags.DEFINE_integer("epochs",50,""" epochs of training""")
     ```
  
  2.启动训练，运行run_train_sh.py
  
- 验证。

  1.测试的时候，运行run_eval_sh.py。

<h2 id="迁移学习指导.md">迁移学习指导</h2>

- 数据集准备。

  数据集要求如下：

  1. 获取数据。

     如果要使用自己的数据集，需要将数据集放到data_url对应目录下。参考代码中的数据集存放路径如下：

     - 训练集：'/3d-bonet-training/3d-bonet/data_s3dis/'
     - 测试集：'/3d-bonet-training/3d-bonet/data_s3dis/'
     
  2. 准确标注类别标签的数据集。
  
  3. 数据集每个类别所占比例大致相同。

- 模型训练。

  参考“模型训练”中训练步骤。

- 模型评估。

  参考“模型训练”中验证步骤。

<h2 id="高级参考.md">高级参考</h2>

## 训练过程<a name="section1589455252218"></a>

1. 通过“模型训练”中的训练指令启动网络训练。

2. 参考脚本的模型存储路径为

3. NPU训练过程打屏信息如下，性能与GPU训练性能持平

4. ```
   ep 0 i 0 test psem 2.5506668 bbvert 1.7146997 l2 0.31039587 ce 1.4159197 siou -0.011615813 bbscore 0.6801287 pmask 1.6942531
   test pred bborder [[20 18  2  9 23 12  7  6 10  0  1  3  4  5  8 11 13 14 15 16 17 19 21 22]
    [20 12 10  7  2  0  1  3  4  5  6  8  9 11 13 14 15 16 17 18 19 21 22 23]
    [20 12 18  2 10  7  0  1  3  4  5  6  8  9 11 13 14 15 16 17 19 21 22 23]
    [20  2  7 10 18 12  0  1  3  4  5  6  8  9 11 13 14 15 16 17 19 21 22 23]]
   2022-06-10 12:36:35.487405: I /home/jenkins/agent/workspace/Compile_GraphEngine_Centos_ARM/tensorflow/tf_adapter/kernels/geop_npu.cc:777] The model has been compiled on the Ascend AI processor, current graph id is: 71
   ep 0  i 0  model saved!
   2022-06-10 12:39:54.497410: I /home/jenkins/agent/workspace/Compile_GraphEngine_Centos_ARM/tensorflow/tf_adapter/kernels/geop_npu.cc:777] The model has been compiled on the Ascend AI processor, current graph id is: 11
   time="2022-06-10T12:40:26+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/plog/plog-123_20220610124009820.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/plog/plog-123_20220610124009820.log, size=20471971, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   time="2022-06-10T12:50:55+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/plog/plog-123_20220610125031319.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/plog/plog-123_20220610125031319.log, size=20949808, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   2022-06-10 12:58:47.620324: I /home/jenkins/agent/workspace/Compile_GraphEngine_Centos_ARM/tensorflow/tf_adapter/kernels/geop_npu.cc:777] The model has been compiled on the Ascend AI processor, current graph id is: 21
   ep 0 i 1 psemce 2.537339 bbvert 2.1974819 l2 0.34096628 ce 1.8715643 siou -0.015048698 bbscore 0.6816362 pmask 1.8165926
   ep 0 i 2 psemce 2.5476513 bbvert 1.7234055 l2 0.28489292 ce 1.4622036 siou -0.023691095 bbscore 0.6700957 pmask 1.7164004
   ep 0 i 3 psemce 2.518845 bbvert 1.8183737 l2 0.3176299 ce 1.5226307 siou -0.021886943 bbscore 0.64723754 pmask 1.702449
   ep 0 i 4 psemce 2.4889574 bbvert 2.0532084 l2 0.27968475 ce 1.8136749 siou -0.040151365 bbscore 0.62453705 pmask 1.7966762
   time="2022-06-10T12:59:23+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610125902916.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610125902916.log, size=20961054, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   time="2022-06-10T12:59:24+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610125904053.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610125904053.log, size=20962481, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   ep 0 i 5 psemce 2.5094154 bbvert 1.3465724 l2 0.25559467 ce 1.136929 siou -0.04595137 bbscore 0.5843524 pmask 1.6026092
   ep 0 i 6 psemce 2.4485474 bbvert 1.3542868 l2 0.23516092 ce 1.1982999 siou -0.07917409 bbscore 0.54570204 pmask 1.6328666
   ep 0 i 7 psemce 2.4193184 bbvert 1.1230415 l2 0.29166543 ce 1.0073298 siou -0.17595372 bbscore 0.5149096 pmask 1.7133747
   ep 0 i 8 psemce 2.407481 bbvert 0.57698786 l2 0.311532 ce 0.57278574 siou -0.3073299 bbscore 0.53012675 pmask 1.7306732
   ep 0 i 9 psemce 2.3338432 bbvert 0.87763995 l2 0.58478516 ce 0.540997 siou -0.2481423 bbscore 0.50399804 pmask 1.6500082
   ep 0 i 10 psemce 2.1569865 bbvert 0.44081318 l2 0.445526 ce 0.31371936 siou -0.3184322 bbscore 0.44488513 pmask 1.668774
   ep 0 i 11 psemce 2.2613268 bbvert 0.51725805 l2 0.27451515 ce 0.52797395 siou -0.28523105 bbscore 0.47453403 pmask 1.6564715
   ep 0 i 12 psemce 1.9155712 bbvert 0.803081 l2 0.31934896 ce 0.7397552 siou -0.25602323 bbscore 0.48089254 pmask 1.7905557
   ep 0 i 13 psemce 2.1010692 bbvert 0.74267006 l2 0.29913348 ce 0.74504983 siou -0.30151322 bbscore 0.42854258 pmask 1.6909289
   time="2022-06-10T13:00:29+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130008419.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130008419.log, size=20935724, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   ep 0 i 14 psemce 2.1867235 bbvert 0.68377507 l2 0.19426893 ce 0.6575149 siou -0.1680088 bbscore 0.42189318 pmask 1.5545664
   time="2022-06-10T13:00:32+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130011514.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130011514.log, size=20959597, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   ep 0 i 15 psemce 2.1962264 bbvert 0.781682 l2 0.22535747 ce 0.77800727 siou -0.22168277 bbscore 0.44671673 pmask 1.649087
   ep 0 i 16 psemce 2.7487967 bbvert 0.7980025 l2 0.19116595 ce 0.8575899 siou -0.25075343 bbscore 0.39205137 pmask 1.6715138
   ep 0 i 17 psemce 2.1888587 bbvert 0.44745958 l2 0.19425532 ce 0.5542447 siou -0.3010404 bbscore 0.4043334 pmask 1.5275421
   ep 0 i 18 psemce 1.8507578 bbvert 0.48269805 l2 0.2616076 ce 0.61425006 siou -0.3931596 bbscore 0.39159626 pmask 1.8648653
   ep 0 i 19 psemce 2.1396754 bbvert 0.6781508 l2 0.31473356 ce 0.65144974 siou -0.28803253 bbscore 0.4086815 pmask 1.5563431
   ep 0 i 20 psemce 2.1086218 bbvert 1.0327187 l2 0.52103376 ce 0.7800773 siou -0.26839238 bbscore 0.36784053 pmask 1.7912049
   ep 0 i 21 psemce 1.8680832 bbvert 0.6263565 l2 0.42449322 ce 0.534631 siou -0.33276775 bbscore 0.37660745 pmask 1.708781
   ep 0 i 22 psemce 2.1292343 bbvert 0.4683337 l2 0.28855133 ce 0.6761838 siou -0.49640146 bbscore 0.28929973 pmask 1.7863662
   ep 0 i 23 psemce 1.9285836 bbvert 0.40720668 l2 0.3020056 ce 0.48248997 siou -0.3772889 bbscore 0.34827214 pmask 1.6003252
   ep 0 i 24 psemce 2.1279323 bbvert 0.30042148 l2 0.21912122 ce 0.38916272 siou -0.30786246 bbscore 0.35444683 pmask 1.6147282
   time="2022-06-10T13:01:38+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130118375.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130118375.log, size=20961662, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   ep 0 i 25 psemce 2.1394076 bbvert 0.60146606 l2 0.20723197 ce 0.619926 siou -0.22569193 bbscore 0.30467314 pmask 1.6113219
   ep 0 i 26 psemce 1.9837791 bbvert 0.15911293 l2 0.17968228 ce 0.35522708 siou -0.37579644 bbscore 0.29461572 pmask 1.6017027
   ep 0 i 27 psemce 1.8076788 bbvert 0.26840055 l2 0.22325476 ce 0.45495364 siou -0.40980786 bbscore 0.27515095 pmask 1.8308233
   ep 0 i 28 psemce 1.5533773 bbvert 0.13066879 l2 0.2573487 ce 0.27704087 siou -0.40372077 bbscore 0.3536865 pmask 1.6670556
   time="2022-06-10T13:02:11+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130149531.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130149531.log, size=20932254, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   ep 0 i 29 psemce 1.8053211 bbvert 0.43888217 l2 0.24704763 ce 0.4748903 siou -0.28305572 bbscore 0.37352657 pmask 1.5670265
   ep 0 i 30 psemce 1.5299962 bbvert 0.1436632 l2 0.1545766 ce 0.36829108 siou -0.37920448 bbscore 0.2729566 pmask 1.585373
   ep 0 i 31 psemce 2.4201548 bbvert 0.48991007 l2 0.23450717 ce 0.6071241 siou -0.35172123 bbscore 0.3765093 pmask 1.7632756
   ep 0 i 32 psemce 1.6769553 bbvert 0.19878134 l2 0.14148079 ce 0.4376379 siou -0.38033733 bbscore 0.26454875 pmask 1.6880915
   ep 0 i 33 psemce 1.2987232 bbvert 0.27658403 l2 0.25227714 ce 0.39357468 siou -0.36926782 bbscore 0.29668525 pmask 1.7253664
   time="2022-06-10T13:02:44+08:00" level=error msg="upload obs file err: upload object with error: open /home/ma-user/modelarts/log/modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130223769.log: no such file or directory, fileName=modelarts-job-116b46c6-0575-4823-a5d4-8a01b7810c54/ascend/process_log/rank_0/device-0/device-123_20220610130223769.log, size=20869253, authLenth=62" file="upload.go:303" Command=obs/upload_by_channels Component=ma-training-toolkit Platform=ModelArts-Service Task=log_url
   ep 0 i 34 psemce 2.4866393 bbvert 0.59633625 l2 0.25912696 ce 0.6074757 siou -0.27026638 bbscore 0.31632286 pmask 1.6286749
   ep 0 i 35 psemce 1.9582666 bbvert 0.28002205 l2 0.1689235 ce 0.4510927 siou -0.33999416 bbscore 0.22037745 pmask 1.6702605
   ep 0 i 36 psemce 1.811698 bbvert 0.4933829 l2 0.25043583 ce 0.51480097 siou -0.2718539 bbscore 0.30991942 pmask 1.5771862
   ep 0 i 37 psemce 1.6618549 bbvert 0.47836852 l2 0.31102103 ce 0.55138683 siou -0.38403934 bbscore 0.3328725 pmask 1.8157715
   ep 0 i 38 psemce 1.9079679 bbvert 0.43432033 l2 0.22495705 ce 0.5115659 siou -0.30220264 bbscore 0.33122012 pmask 1.5347129
   ep 0 i 39 psemce 1.6006442 bbvert 0.24499816 l2 0.21418393 ce 0.42552269 siou -0.39470845 bbscore 0.3117543 pmask 1.6272228
   ep 0 i 40 psemce 1.6767414 bbvert 0.28237173 l2 0.30173686 ce 0.34201124 siou -0.36137637 bbscore 0.31259435 pmask 1.6602752
   ep 0 i 41 psemce 2.155477 bbvert 0.29884484 l2 0.16350603 ce 0.4825609 siou -0.34722206 bbscore 0.27073908 pmask 1.5562432
   ep 0 i 42 psemce 1.7484361 bbvert 0.13059133 l2 0.23228115 ce 0.38949072 siou -0.49118054 bbscore 0.2278314 pmask 1.9159931
   ep 0 i 43 psemce 1.2524122 bbvert -0.023730993 l2 0.3203728 ce 0.21102439 siou -0.55512816 bbscore 0.24068835 pmask 2.14660
   ```
   

