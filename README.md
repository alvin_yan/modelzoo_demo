# modelzoo_demo

#### 介绍
modelzoo提交源码规范示例

#### 软件架构
软件架构说明


#### 安装教程

1.  下载仓库内容到个人本地windows环境
2.  配置modelarts pycharm toolkit插件  
![输入图片说明](image_pycharm_toolkit.png)

#### 使用说明

1.  modelarts_entry_xxx.py文件是modelarts场景下执行训练的入口文件
2.  modelarts_entry_perf.py文件用于性能测试，会调用test/train_performance_1p.sh脚本
3.  modelarts_entry_acc.py文件用于精度测试，会调用test/train_full_1p.sh脚本
4.  modelarts_entry_perf.py和modelarts_entry_acc.py无需做任何改动
5.  只需要对train_performance_1p.sh和train_full_1p.sh针对网络模型做有限的设置  
    (1) train_full_1p.sh - 用于执行精度训练  
    (2) train_performance_1p.sh - 用于执行性能训练  
    (3) 脚本必须放置在网络首层目录下的test目录下（test目录请自行创建）  

